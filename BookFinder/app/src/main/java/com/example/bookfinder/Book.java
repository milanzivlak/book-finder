package com.example.bookfinder;

import java.io.Serializable;
import java.util.List;

public class Book implements Serializable {
    // Volume info about book

    public static final String TABLE_NAME = "book";
    public static final String FIELD_ID = "id";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_AUTHORS = "authors";
    public static final String FIELD_PUBLISHED = "published";
    public static final String FIELD_PAGES = "pages";
    public static final String FIELD_DESCRIPTION = "description";
    public static final String FIELD_THUMBNAIL = "thumbnail";


    private String id;
    private String title;
    private String authors;
    private String published;
    private String pages;
    private String description;
    private String thumbnail;





    /*
     * Details:
     * publisher, publishDate, description, industryIndentifiers
     * pageCount, dimensions, printType, mainCategory, categories
     * averageRating, ratingsCount, contentVersion, imageLinks
     * language, infoLink, canonicalVolumeLink, saleInfo, retailPrice
     * buyLink, accesInfo, pdf, accesViewStatus
     * */


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Book() {
    }

    public Book(String id, String title, String authors, String published, String pages, String description, String thumbnail) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.published = published;
        this.pages = pages;
        this.description = description;
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", authors='" + authors + '\'' +
                ", published='" + published + '\'' +
                ", pages='" + pages + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
