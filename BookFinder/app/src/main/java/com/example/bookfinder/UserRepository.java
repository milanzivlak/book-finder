package com.example.bookfinder;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private Database database;

    public UserRepository(Database db){this.database = db;}

    public void addUser(String username, String password){
        SQLiteDatabase db = database.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(User.FIELD_USERNAME, username);
        cv.put(User.FIELD_PASSWORD, password);
        // Insert novog User-a u tabelu
        db.insert(User.TABLE_NAME, null, cv);

    }

    // Omoguciti korisniku brisanje svog naloga
    public int deleteUser(String username){
        int numDeleted = 0;
        SQLiteDatabase db = database.getWritableDatabase();
        numDeleted = db.delete(User.TABLE_NAME, User.FIELD_USERNAME + "=?", new String[] {String.valueOf(username)});

        return numDeleted;
    }

    // Dobavljanje korisnika preko username-a
    // Koristi se prilikom registracije i login-a
    public User getUserByUsername(String username){
        SQLiteDatabase db = database.getReadableDatabase();

        // SELECT * FROM user WHERE username = ?

        String query = String.format("SELECT * FROM %s WHERE %s = ?", User.TABLE_NAME, User.FIELD_USERNAME);
        Cursor result = db.rawQuery(query, new String[]{String.valueOf(username)});
        if(result.moveToFirst()){
            String usernameFound = result.getString(result.getColumnIndex(User.FIELD_USERNAME));
            String password = result.getString(result.getColumnIndex(User.FIELD_PASSWORD));

            User user = new User(); // Prazan konstruktor je pozvan jer korisnik za sada ima atribut wishlist
            user.setUsername(usernameFound);
            user.setPassword(password);
            return user;


        }else{
            return null;
        }
    }

    public ArrayList<User> getAllUsers(){
        SQLiteDatabase db = database.getReadableDatabase();
        String query = String.format("SELECT * FROM %s", User.TABLE_NAME);
        Cursor result = db.rawQuery(query, null);
        result.moveToFirst();
        ArrayList<User> list = new ArrayList<User>(result.getCount()); // Kreiranje sa onoliko elemenata koliko je pronadjeno

        while(!result.isAfterLast()){
            String username = result.getString(result.getColumnIndex(User.FIELD_USERNAME));
            String password = result.getString(result.getColumnIndex(User.FIELD_PASSWORD));

            User newUser= new User();
            newUser.setUsername(username);
            newUser.setPassword(password);
            list.add(newUser);
            result.moveToNext();
        }

        return list;

    }


}
