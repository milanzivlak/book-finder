package com.example.bookfinder;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable {
    public static final String TABLE_NAME = "user";
    public static final String FIELD_USERNAME = "username";
    public static final String FIELD_PASSWORD = "password";


    private String username;
    private String password;
    private ArrayList<Book> wishlist;



    public String getUsername(){ return this.username;}
    public void setUsername(String username){ this.username = username;}

    public String getPassword(){return this.password;}
    public void setPassword(String password){this.password = password;}

    public ArrayList<Book> getWishlist() {
        return wishlist;
    }
    public void  setWishlist(ArrayList<Book> wishlist) {
        this.wishlist = wishlist;
    }
    public void addBook(Book b){this.wishlist.add(b);}
    public void removeBook(Book b){this.wishlist.remove(b);}

    User(){

    }

    User(String username, String password, ArrayList<Book> wishlist){
        this.username = username;
        this.password = password;
        this.wishlist = wishlist;
    }
}
