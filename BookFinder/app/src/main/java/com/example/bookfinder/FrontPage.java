package com.example.bookfinder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class FrontPage extends AppCompatActivity {
    private static final String TAG = FrontPage.class.getSimpleName();
    private EditText bookInput;
    private TextView authorText, titleText;
    private String publishedText, pagesText, descriptionText, imageUrl, idText;
    private ImageView image;
    FetchBook fb;
    User logged;
    UserRepository usersRepo;
    BookRepository booksRepo;
    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        logged = (User)getIntent().getSerializableExtra("logged");


        bookInput = findViewById(R.id.searchInput);
        authorText = findViewById(R.id.authorValue);
        titleText = findViewById(R.id.titleValue);
        image = findViewById(R.id.thumbnail);
        publishedText = "";
        pagesText = "";
        descriptionText = "";



        Button search =  findViewById(R.id.searchButton);
        search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //setContentView(R.layout.activity_main);
                searchBooks(v);
            }
        });


        TabLayout tabs = (TabLayout)findViewById(R.id.navigation);
        TabLayout.Tab t1 = tabs.getTabAt(0);
        int noBooks = logged.getWishlist().size();
        String text = String.format("Wishlist (%s)", Integer.toString(noBooks));
        t1.setText(text);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                String[] s = tab.getText().toString().split(" ", 2);
                System.out.println(s[0]);
                switch (s[0]) {
                    case "Wishlist":
                        showWishlist();
                        break;

                    case "Logout":
                        changeActivity();
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                switch (tab.getText().toString()) {
                    case "yourTabTitle":
                        //todo your code
                        break;
                }
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                String[] s = tab.getText().toString().split(" ", 2);
                switch (s[0]) {
                    case "Wishlist":
                        showWishlist();
                        break;
                }
            }
        });



    }

    private void showWishlist(){
        Intent i = new Intent(this, WishlistActivity.class);



        Bundle extras = new Bundle();
        extras.putSerializable("logged", logged);

        i.putExtras(extras);
        startActivity(i);
        finish();
    }

    private void changeActivity(){
        Intent i = new Intent(this, MainActivity.class);

        String username = "";
        String password = "";

        Bundle extras = new Bundle();
        extras.putString("username", username);
        extras.putString("password", password);
        i.putExtras(extras);
        startActivity(i);
        finish();

    }

    private void searchBooks(View view){
        String queryString = bookInput.getText().toString();
        Log.i(TAG, "Searched: " + queryString);
        fb = new FetchBook(titleText, authorText, image, publishedText, pagesText, descriptionText, imageUrl, idText);
        fb.execute(queryString);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bookActivity();
            }
        });


    }

    private void bookActivity(){
        Intent intent = new Intent(this, BookActivity.class);

        Bundle extras = new Bundle();
        extras.putString("id", fb.getIdText());
        extras.putString("title", titleText.getText().toString());
        extras.putString("authors", authorText.getText().toString());
        extras.putString("published", fb.getPublishedText());
        extras.putString("pages", fb.getPagesText());
        extras.putString("description", fb.getDescriptionText());
        extras.putString("thumbnail", fb.getImageUrl());
        extras.putSerializable("logged", logged);

        //extras.putSerializable("users", users);

        intent.putExtras(extras);
        startActivity(intent);
        finish();
    }
}