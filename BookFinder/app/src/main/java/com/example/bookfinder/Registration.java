package com.example.bookfinder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class Registration extends AppCompatActivity {

    ArrayList<User> users;
    User newUser;
    Database db;
    UserRepository userRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        db = new Database(this);
        userRepository = new UserRepository(db);

        TabLayout tabs = (TabLayout)findViewById(R.id.registrationTab);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getText().toString()) {
                    case "Home":
                        returnToHome();
                        break;

                    case "Register":
                        checkRegistration();
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                switch (tab.getText().toString()) {
                    case "yourTabTitle":
                        //todo your code
                        break;
                }
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Home":
                        returnToHome();
                        break;

                    case "Register":
                        checkRegistration();
                        break;
                }
            }
        });
    }
    private void returnToHome(){
        Intent intent = new Intent(this, MainActivity.class);

        startActivity(intent);
        finish();
    }

    private void checkRegistration(){
        EditText newUsername = findViewById(R.id.newUsernameInput);
        String username = newUsername.getText().toString();

        if(username.equals("")){
            newUsername.setError("Please enter username");
        }
        EditText newPassword = findViewById(R.id.newPasswordInput);

        String password = newPassword.getText().toString();

        if(password.equals("")){
            newPassword.setError("Please enter password");
        }


        users = (ArrayList<User>)getIntent().getSerializableExtra("users");

        if(!username.equals("") && !password.equals("")){
            newUser = new User();
            newUser.setUsername(username);
            newUser.setPassword(password);



            //System.out.println("New user added: " + username + password);

            if(checkUsername()){
                Toast toast =  Toast.makeText(getApplicationContext(),"That username has already been taken!",Toast.LENGTH_SHORT);
                toast.show();
            }else{
                Toast toast =  Toast.makeText(getApplicationContext(),"Your registration is successful!",Toast.LENGTH_SHORT);
                toast.show();

                userRepository.addUser(username, password);
                //users.add(newUser);
                returnToHome();
            }
        }






    }


    private boolean checkUsername(){
        boolean found = false;
        users = userRepository.getAllUsers();
        for(User u : users){
            if(u.getUsername().equals(newUser.getUsername())){
                found = true;
            }
        }
        return found;
    }
}