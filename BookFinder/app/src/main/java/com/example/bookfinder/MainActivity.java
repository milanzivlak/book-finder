package com.example.bookfinder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<User> users = new ArrayList<User>();
    User logged = new User();
    UserRepository usersRepo;
    BookRepository booksRepo;
    Database db;



    private void addUser(User u){
        users.add(u);
    }

    private void deleteUser(User u){
        users.remove(u);
    }

    private boolean checkLoginData(){
        users = usersRepo.getAllUsers();

        EditText inputUsername = findViewById(R.id.usernameInput);
        String username = inputUsername.getText().toString();

        EditText inputPassword = findViewById(R.id.passwordInput);
        String password = inputPassword.getText().toString();


        if(username.equals("")){
            inputUsername.setError("Enter username");
        }
        if(password.equals("")){
            inputPassword.setError("Enter password");
        }

        for(User u : users){
            if(u.getUsername().equals(username) && u.getPassword().equals(password)){
                logged.setUsername(username);
                logged.setPassword(password);
                //logged.setWishlist(u.getWishlist());
                ArrayList<Book> wish = db.getBooksFromWishlist(logged.getUsername());
                //ArrayList<Book> wish = new ArrayList<Book>();
                System.out.println(wish.size());
                logged.setWishlist(wish);
                return true;
            }
        }

        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        initDatabase();

        //users = (ArrayList<User>)getIntent().getSerializableExtra("users");

        if(users == null){
            users = new ArrayList<User>();
            users = usersRepo.getAllUsers(); // Dobavljanje svih User-a iz db
            //populateUsers();
        }
        //users = (ArrayList<User>)getIntent().getSerializableExtra("users");

        checkLogout();


        System.out.println(users.size());
        Button login =  findViewById(R.id.loginButton);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(checkLoginData() == true){
                    changeActivity();

                }else{
                    Toast toast =  Toast.makeText(getApplicationContext(),"Wrong username or password. Try again.",Toast.LENGTH_SHORT);
                    toast.show();
                };
            }
        });

        Button registration =  findViewById(R.id.registrationButton);
        registration.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                registrationActivity();

            }
        });

    }

    private void changeActivity(){
        Intent i = new Intent(this, FrontPage.class);

        Bundle extras = new Bundle();
        extras.putString("username", logged.getUsername());
        extras.putString("password", logged.getPassword());
        extras.putSerializable("logged", logged);

        i.putExtras(extras);
        startActivity(i);
        finish();

    }

    private void registrationActivity(){
        Intent i = new Intent(this, Registration.class);

        Bundle extras = new Bundle();
        extras.putSerializable("users", users);

        i.putExtras(extras);
        startActivity(i);
        finish();
    }


    private void checkLogout(){
        logged.setUsername(getIntent().getStringExtra("username"));
        logged.setPassword(getIntent().getStringExtra("password"));
    }

    private void checkRegistration(){
        users = (ArrayList<User>)getIntent().getSerializableExtra("users");

    }

    private void initDatabase(){
        db = new Database(this);
        usersRepo = new UserRepository(db);
        booksRepo = new BookRepository(db);
//        booksRepo.deleteAllBooks();
//        db.deleteAllFromWishlist();

        //usersRepo.addUser("darko", "123");
        User u = usersRepo.getUserByUsername("darko");
        System.out.println(u.getUsername());
    }


}