package com.example.bookfinder;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProvider;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.URL;

public class FetchBook extends AsyncTask<String, Void, String> {


    private String idText;
    private TextView titleText;
    private TextView authorText;
    private ImageView image;
    private String publishedText;
    private String pagesText;
    private String descriptionText;
    private String imageUrl;

    public String getIdText() {
        return idText;
    }

    public void setIdText(String idText) {
        this.idText = idText;
    }

    public TextView getTitleText() {
        return titleText;
    }

    public void setTitleText(TextView titleText) {
        this.titleText = titleText;
    }

    public TextView getAuthorText() {
        return authorText;
    }

    public void setAuthorText(TextView authorText) {
        this.authorText = authorText;
    }

    public ImageView getImage() {
        return image;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }

    public String getPublishedText() {
        return publishedText;
    }

    public void setPublishedText(String publishedText) {
        this.publishedText = publishedText;
    }

    public String getPagesText() {
        return pagesText;
    }

    public void setPagesText(String pagesText) {
        this.pagesText = pagesText;
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(String descriptionText) {
        this.descriptionText = descriptionText;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public FetchBook(TextView titleText, TextView authorText, ImageView image, String publishedText, String pagesText, String descriptionText, String imageUrl, String idText){
        this.titleText = titleText;
        this.authorText = authorText;
        this.image = image;
        this.publishedText = publishedText;
        this.pagesText = pagesText;
        this.descriptionText = descriptionText;
        this.imageUrl = imageUrl;
        this.idText = idText;
    }
    @Override
    protected String doInBackground(String... strings) {

        try{
            return NetworkUtils.getBookInfo(strings[0]);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "No results found";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(s);
            JSONArray itemsArray = jsonObject.getJSONArray("items");

            for(int i = 0; i < itemsArray.length(); i++){
                JSONObject book = itemsArray.getJSONObject(i);
                String title = null;
                String authors = null;
                String thumbnail = null;
                String published = null;
                String description = null;
                String pages = null;

                setIdText(book.getString("id"));
                JSONObject volumeInfo = book.getJSONObject("volumeInfo");
                JSONObject imageLinks = volumeInfo.getJSONObject("imageLinks");
                //JSONObject pdf = volumeInfo.getJSONObject("pdf");

                //String pdfLink = pdf.getString("acsTokenLink");
                try{
                    title = volumeInfo.getString("title");
                    authors = volumeInfo.getString("authors");
                    thumbnail = imageLinks.getString("thumbnail");
                    published = volumeInfo.getString("publishedDate");
                    description = volumeInfo.getString("description");
                    pages = volumeInfo.getString("pageCount");


                }catch(Exception e){
                    e.printStackTrace();
                }

                if(title != null && authors != null){
                    titleText.setText(title);
                    authorText.setText(authors);
                    setImageUrl(thumbnail);
                    Picasso.get().load(thumbnail).into(image);
                    setPublishedText(published);
                    setPagesText(pages);
                    setDescriptionText(description);
//                    System.out.println(descriptionText);
//                    System.out.println(publishedText);
//                    System.out.println(pagesText);

                    return;
                }
            }
            titleText.setText("No results found");
            authorText.setText("");
            image.setImageResource(R.drawable.blank);
        } catch (Exception e) {
            titleText.setText("No results found");
            authorText.setText("");
            image.setImageResource(R.drawable.blank);
            e.printStackTrace();
        }

    }
}
