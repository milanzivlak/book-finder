package com.example.bookfinder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

public class WishlistActivity extends AppCompatActivity {
    User logged;
    LinearLayout mainLayout;
    UserRepository usersRepo;
    BookRepository booksRepo;
    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        db = new Database(this);
        logged = (User)getIntent().getSerializableExtra("logged");
        // Dobavljanje db i repository-a
//        usersRepo = (UserRepository)getIntent().getSerializableExtra("usersRepo");
//        booksRepo = (BookRepository) getIntent().getSerializableExtra("booksRepo");
//        db = (Database) getIntent().getSerializableExtra("database");
        mainLayout = findViewById(R.id.wishlistLinearLayout);

        TabLayout tabs = (TabLayout)findViewById(R.id.wishlistTab);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Home":
                        returnToHome();

                    case "Logout":
                        changeActivity();
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "yourTabTitle":
                        //todo your code
                        break;
                }
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Home":
                        returnToHome();
                        break;
                }
            }
        });
        generateBooks();
    }

    
    private void setupTabs(){
        TabLayout tabs = (TabLayout)findViewById(R.id.wishlistTab);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Home":
                        returnToHome();

                    case "Logout":
                        changeActivity();
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "yourTabTitle":
                        //todo your code
                        break;
                }
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Home":
                        returnToHome();
                        break;
                }
            }
        });
    }
    private void changeActivity(){
        Intent i = new Intent(this, MainActivity.class);

        String username = "";
        String password = "";

        Bundle extras = new Bundle();
        extras.putString("username", username);
        extras.putString("password", password);
        i.putExtras(extras);
        startActivity(i);
        finish();

    }

    private void returnToHome(){
        Intent intent = new Intent(this, FrontPage.class);
        Bundle extras = new Bundle();
        extras.putSerializable("logged", logged);
        intent.putExtras(extras);
        startActivity(intent);
        finish();
    }

    private void fillBookView(Book b, View v){
        TextView bookTitle = v.findViewById(R.id.singleBookTitle);
        TextView bookAuthors = v.findViewById(R.id.singleBookAuthors);
        TextView bookPublished = v.findViewById(R.id.singleBookPublished);
        TextView bookPages = v.findViewById(R.id.singleBookPages);
        ImageView img = v.findViewById(R.id.singleBookImage);

        Picasso.get().load(b.getThumbnail()).into(img);
        bookTitle.setText(b.getTitle());
        bookAuthors.setText(b.getAuthors());
        bookPublished.setText(b.getPublished());
        bookPages.setText(b.getPages());
        bookTitle.setMovementMethod(new ScrollingMovementMethod());
        //bookAuthors.setMovementMethod(new ScrollingMovementMethod());
        Button delete = v.findViewById(R.id.singleBookDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                int numbDel = db.deleteBookFromWishlist(b.getId());
                if(numbDel == 1){
                    Toast toast =  Toast.makeText(getApplicationContext(),"The book is deleted!",Toast.LENGTH_SHORT);
                    toast.show();
                }
                //logged.getWishlist().remove(b);
                logged.setWishlist(db.getBooksFromWishlist(logged.getUsername()));
                setContentView(R.layout.activity_wishlist);
                mainLayout = findViewById(R.id.wishlistLinearLayout);
                setupTabs();
                generateBooks();
            }
        });

        mainLayout.addView(v);
    }

    private void generateBooks(){
        LayoutInflater inflater = getLayoutInflater();
        // Potrebno je dobaviti sve knjige iz tabele preko logged.getUsername() i prolaziti kroz nju
        // ArrayList<Book> wishlist = database.getBooksFromWishlist(logged.getUsername())
        for(Book b : logged.getWishlist()){
            View v = inflater.inflate(R.layout.book_details, mainLayout, false);

            fillBookView(b, v);

        }
    }
}
