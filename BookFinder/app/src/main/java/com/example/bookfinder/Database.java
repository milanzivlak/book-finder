package com.example.bookfinder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Database extends SQLiteOpenHelper implements Serializable {
    public static final String DATABASE_NAME="baza.sqlite";
    public static final String WISHLIST_NAME = "wishlist";
    public static final String WISHLIST_USERNAME = "user_username";
    public static final String WISHLIST_BOOK_ID = "book_id";
    public Database(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String SQL = String.format("CREATE TABLE IF NOT EXISTS %s (%s TEXT PRIMARY KEY , %s TEXT);",
                User.TABLE_NAME, User.FIELD_USERNAME, User.FIELD_PASSWORD);

        db.execSQL(SQL);

        String SQL_BOOK = String.format("CREATE TABLE IF NOT EXISTS %s (%s TEXT PRIMARY KEY , %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT);",
                Book.TABLE_NAME, Book.FIELD_ID, Book.FIELD_TITLE, Book.FIELD_AUTHORS, Book.FIELD_PUBLISHED, Book.FIELD_PAGES, Book.FIELD_DESCRIPTION, Book.FIELD_THUMBNAIL);

        db.execSQL(SQL_BOOK);

        String SQL_WISHLIST = String.format("CREATE TABLE IF NOT EXISTS %s (%s TEXT REFERENCES %s(%s), %s TEXT REFERENCES %s(%s));",
                WISHLIST_NAME, WISHLIST_USERNAME, User.TABLE_NAME, User.FIELD_USERNAME,  WISHLIST_BOOK_ID, Book.TABLE_NAME, Book.FIELD_ID);
        db.execSQL(SQL_WISHLIST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //implementacija promene baze iz jedne verzije u drugu
        //ovde bi trebalo implementirati tranziciju
        //db.execSQL(String.format("DROP TABLE IF EXISTS %s;", User.TABLE_NAME));
        //kreiranje nove tabele
        //onCreate(db);
        System.out.println("Potrebno je implementirati onUpgrade!");
    }

    // Brisanje knjige iz wishlist-a od korisnika
    public int deleteBookFromWishlist(String id){
        int numDeleted = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        numDeleted = db.delete(WISHLIST_NAME, WISHLIST_BOOK_ID + "=?", new String[] {String.valueOf(id)});

        return numDeleted;
    }

    public void deleteAllFromWishlist(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(WISHLIST_NAME, null, new String[]{});
    }
    // Dodavanje knjige u wishlist-u od korisnika
    public void addBookToWishlist(String username, String id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(WISHLIST_USERNAME, username);
        cv.put(WISHLIST_BOOK_ID, id);


        db.insert(WISHLIST_NAME, null, cv);

    }

    public int getBookFromWishlist(String username, String id){
        SQLiteDatabase db = this.getReadableDatabase();

        // SELECT * FROM user WHERE username = ?

        String query = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ?", WISHLIST_NAME, WISHLIST_USERNAME, WISHLIST_BOOK_ID);
        Cursor result = db.rawQuery(query, new String[]{username, id});
        if(result.moveToFirst()){

            return 1;


        }else{
            return 0;
        }
    }
    public ArrayList<Book> getBooksFromWishlist(String username){
        SQLiteDatabase db = this.getReadableDatabase();
        BookRepository br = new BookRepository(this);

        // Prvo dobavljamo listu svih ID-eva knjiga koje ima neki User
        String queryBooks = String.format("SELECT * FROM %s WHERE %s = ?", WISHLIST_NAME, WISHLIST_USERNAME);
        Cursor result = db.rawQuery(queryBooks, new String[]{String.valueOf(username)});
        result.moveToFirst();
        ArrayList<String> idList = new ArrayList<String>(result.getCount()); // Kreiranje sa onoliko elemenata koliko je pronadjeno

        while(!result.isAfterLast()){
            String id = result.getString(result.getColumnIndex(WISHLIST_BOOK_ID));

            idList.add(id);
            result.moveToNext();
        }

        System.out.println("Number of books: " +  idList.size());
        // Prolazimo kroz sve pokupljene ID-eve i iz tabele Book dobavljamo knjige sa tim ID-evima
        ArrayList<Book> usersBooks = new ArrayList<Book>();

        //String query = String.format("SELECT * FROM %s WHERE %s = ?", Book.TABLE_NAME, Book.FIELD_ID);

        for(String s : idList){
            Book b = br.getBookById(s);
            usersBooks.add(b);
        }

        return usersBooks;
    }
}
