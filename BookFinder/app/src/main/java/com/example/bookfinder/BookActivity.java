package com.example.bookfinder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.text.method.ScrollingMovementMethod;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class BookActivity extends AppCompatActivity {

    private String id, title, authors, published, pages, description, thumbnail;
    User logged;
    TabLayout.Tab t1;
    UserRepository usersRepo;
    BookRepository booksRepo;
    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        logged = (User)getIntent().getSerializableExtra("logged");
        db = new Database(this);
        booksRepo = new BookRepository(db);
        usersRepo = new UserRepository(db);

        checkBookData();

        TabLayout tabs = (TabLayout)findViewById(R.id.bookActivityLayout);
        t1 = tabs.getTabAt(1);
        int noBooks = logged.getWishlist().size();
        String text = String.format("Wishlist (%s)", Integer.toString(noBooks));
        t1.setText(text);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                String[] s = tab.getText().toString().split(" ", 2);
                switch (s[0]) {
                    case "Home":
                        returnToHome();
                    case "Wishlist":
                        showWishlist();
                        break;

                    case "Logout":
                        changeActivity();
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "yourTabTitle":
                        //todo your code
                        break;
                }
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Home":
                        returnToHome();
                        break;
                }
            }
        });
    }
    private void changeActivity(){
        Intent i = new Intent(this, MainActivity.class);

        String username = "";
        String password = "";

        Bundle extras = new Bundle();
        extras.putString("username", username);
        extras.putString("password", password);
        i.putExtras(extras);
        startActivity(i);
        finish();

    }

    private void showWishlist(){
        Intent i = new Intent(this, WishlistActivity.class);


        Bundle extras = new Bundle();
        extras.putSerializable("logged", logged);
        i.putExtras(extras);
        startActivity(i);
        finish();
    }

    private void checkBookData(){
        id = getIntent().getStringExtra("id");
        title = getIntent().getStringExtra("title");
        authors = getIntent().getStringExtra("authors");
        published = getIntent().getStringExtra("published");
        pages = getIntent().getStringExtra("pages");
        description = getIntent().getStringExtra("description");
        thumbnail = getIntent().getStringExtra("thumbnail");


        TextView titleValue = findViewById(R.id.bookTitleValue);
        TextView authorsValue = findViewById(R.id.bookAuthorsValue);
        TextView publishedValue = findViewById(R.id.bookPublishValue);
        TextView pagesValue = findViewById(R.id.bookPagesValue);
        TextView descriptionValue = findViewById(R.id.bookDescriptionValue);
        ImageView image = findViewById(R.id.bookImage);
        ImageView wishlist = findViewById(R.id.wishlistIcon);

        titleValue.setText(title);
        authorsValue.setText(authors);
        Picasso.get().load(thumbnail).into(image);
        publishedValue.setText(published);
        pagesValue.setText(pages);
        descriptionValue.setText(description);
        descriptionValue.setMovementMethod(new ScrollingMovementMethod());



        wishlist.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addToWishlist();
            }
        });
    }

    private void returnToHome(){
        Intent intent = new Intent(this, FrontPage.class);
        Bundle extras = new Bundle();
        extras.putSerializable("logged", logged);
        intent.putExtras(extras);
        startActivity(intent);
        finish();
    }

    private void addToWishlist(){

        // Nedostaje wishlist u koju se smjesta.

        try{
            Book book = new Book(id, title, authors, published, pages, description, thumbnail);
            Boolean found = false;
            if (db.getBookFromWishlist(logged.getUsername(), book.getId()) == 1){
                found = true;
                Toast toast =  Toast.makeText(getApplicationContext(),"That book is already in your wishlist!",Toast.LENGTH_SHORT);
                toast.show();
                //System.out.println("That book is already in your's wishlist!");

            }
            if(!found){
                // Potrebno je dodati novu knjigu i takodje dodati novi red u tabelu Wishlist

                User u = usersRepo.getUserByUsername("darko");
                //System.out.println(u.getUsername());
                if(booksRepo.getBookById(id) == null){
                    booksRepo.addBook(id, title, authors, published, pages, description, thumbnail);
                }

                db.addBookToWishlist(logged.getUsername(), book.getId());
                ArrayList<Book> bks = db.getBooksFromWishlist(logged.getUsername());
                logged.setWishlist(db.getBooksFromWishlist(logged.getUsername())); // Update-ovanje wishlist-a
                Toast toast =  Toast.makeText(getApplicationContext(),"The book has been successfully added!",Toast.LENGTH_SHORT);
                toast.show();
                System.out.println("Number of books in user's wishlist: " + logged.getWishlist().size());

                int noBooks = logged.getWishlist().size();
                String text = String.format("Wishlist (%s)", Integer.toString(noBooks));
                t1.setText(text);
        }

        }catch (Exception e){
            e.printStackTrace();
            Toast toast =  Toast.makeText(getApplicationContext(),"Invalid search!",Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}