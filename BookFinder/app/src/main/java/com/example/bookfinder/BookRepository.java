package com.example.bookfinder;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.Serializable;

public class BookRepository{
    private Database database;

    public BookRepository(Database db){this.database = db;}

    public void addBook(String id, String title, String authors, String published, String pages, String description, String thumbnail){
        SQLiteDatabase db = database.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(Book.FIELD_ID, id);
        cv.put(Book.FIELD_TITLE, title);
        cv.put(Book.FIELD_AUTHORS, authors);
        cv.put(Book.FIELD_PUBLISHED, published);
        cv.put(Book.FIELD_PAGES, pages);
        cv.put(Book.FIELD_DESCRIPTION, description);
        cv.put(Book.FIELD_THUMBNAIL, thumbnail);

        // Insert nove Knjige u tabelu
        db.insert(Book.TABLE_NAME, null, cv);

    }

    // Funkcija za brisanje knjige
    public int deleteBook(String id){
        int numDeleted = 0;
        SQLiteDatabase db = database.getWritableDatabase();
        numDeleted = db.delete(Book.TABLE_NAME, Book.FIELD_ID + "=?", new String[] {String.valueOf(id)});

        return numDeleted;
    }

    public void deleteAllBooks(){
        SQLiteDatabase db = database.getWritableDatabase();
        db.delete(Book.TABLE_NAME, null, new String[]{});
    }

    // Dobavljanje knjige preko ID-a
    public Book getBookById(String id){
        SQLiteDatabase db = database.getReadableDatabase();

        // SELECT * FROM book WHERE id = ?

        String query = String.format("SELECT * FROM %s WHERE %s = ?", Book.TABLE_NAME, Book.FIELD_ID);
        Cursor result = db.rawQuery(query, new String[]{String.valueOf(id)});
        if(result.moveToFirst()){
            String idFound = result.getString(result.getColumnIndex(Book.FIELD_ID));
            String title = result.getString(result.getColumnIndex(Book.FIELD_TITLE));
            String authors = result.getString(result.getColumnIndex(Book.FIELD_AUTHORS));
            String published = result.getString(result.getColumnIndex(Book.FIELD_PUBLISHED));
            String pages = result.getString(result.getColumnIndex(Book.FIELD_PAGES));
            String description = result.getString(result.getColumnIndex(Book.FIELD_DESCRIPTION));
            String thumbnail = result.getString(result.getColumnIndex(Book.FIELD_THUMBNAIL));

            Book book = new Book(idFound, title, authors, published, pages, description, thumbnail);
            return book;


        }else{
            return null;
        }
    }
}
